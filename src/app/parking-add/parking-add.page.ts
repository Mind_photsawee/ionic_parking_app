import { Component, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';
import { IParking } from '../../models/parking';

import { ParkingProviderService } from '../parking-provider.service';

// มีการรับค่าผ่านมาจาก Route ต้อง Import ตัวนี้ด้วย
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-parking-add',
  templateUrl: './parking-add.page.html',
  styleUrls: ['./parking-add.page.scss'],
})
export class ParkingAddPage implements OnInit {

  parking: Array<IParking> = [];

  constructor(public navCtrl: NavController, private route: ActivatedRoute, private router: Router, private parkingProvider: ParkingProviderService) {
    this.parking[0] = { id:0, name:'' }
  }

  save(){
    console.log("This is save mode");

    this.parkingProvider.addParking(this.parking[0]).then((data: any) => {
      if(data.ok){
        this.parking[0] = { id:0, name:'' }
        console.log('Add parking success');
      }
    },
    error => {
      console.log('Error save data!', error);
    })
  }

  ngOnInit() {
  }

}
