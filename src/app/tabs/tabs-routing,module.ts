import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { TabsPage } from './tabs.page';
import { from } from 'rxjs';

const routes: Routes = [
    {
        path: 'tabs',
        component: TabsPage,
        children:
            [
                {
                    path: 'tab1',
                    children:
                        [
                            {
                                path: '',
                                loadChildren: '../home/home.module#HomePageModule'
                            }
                        ]
                },
                {
                    path: 'tab2',
                    children:
                        [
                            {
                                path: '',
                                loadChildren: '../parking-list/parking-list.module#ParkingListPageModule'
                            }
                        ]
                },
                {
                    path: 'tab3',
                    children:
                        [
                            {
                                path: '',
                                loadChildren: '../map/map.module#MapPageModule'
                            }
                        ]
                },
                {
                    path: 'tab4',
                    children:
                        [
                            {
                                path: '',
                                loadChildren: '../qrcode/qrcode.module#QrcodePageModule'
                            }
                        ]
                },
                {
                    path: 'tab5',
                    children:
                        [
                            {
                                path: '',
                                loadChildren: '../setting/setting.module#SettingPageModule'
                            }
                        ]
                },
                {
                    path: '',
                    redirectTo: '/tabs/tab1',
                    pathMatch: 'full'
                },
            ]
    },
    {
        path: '',
        redirectTo: '/tabs/tab1',
        pathMatch: 'full'
    }
];

@NgModule({
    imports: [
        RouterModule.forChild(routes)
    ],
    exports: [RouterModule]
})
export class TabsPageRoutingModule { }
