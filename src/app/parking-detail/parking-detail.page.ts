import { Component, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';
import { IParking } from '../../models/parking';

import { ParkingProviderService } from '../parking-provider.service';

// มีการรับค่าผ่านมาจาก Route ต้อง Import ตัวนี้ด้วย
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-parking-detail',
  templateUrl: './parking-detail.page.html',
  styleUrls: ['./parking-detail.page.scss'],
})
export class ParkingDetailPage implements OnInit {

  parking: Array<IParking> = [];

  q: any;

  constructor(public navCtrl: NavController, private route: ActivatedRoute, private router: Router, private parkingProvider: ParkingProviderService) {
    let p = this.route.snapshot.paramMap.get('p');
    console.log(p);
    this.q = p;
    this.doSearch();
  }

  doSearch(){
    this.parkingProvider.getParkingID(this.q).then((data: any) => {
      this.parking = data.rows;
      // console.log(this.parking);
      // console.log('name = ', this.parking[0].name);
    },
    error => {
      console.error('Error get data!', error);
    })
  }

  ngOnInit() {
  }

}
